# Digital Currency - Drachma - DRX

## Digital Currency

Digital currency is any currency available exclusively in electronic form. For a currency to be digital, all electronic versions of the currency must be applicable to the financial systems of most countries. What differentiates digital currency from the electronic currency that already exists in bank accounts around the world is that digital currency never takes a physical form, i.e. paper money. At the moment, anyone can go to an ATM and easily convert the electronic record of their coins into physical notes. Digital currency, however, never leaves a computer network and is exchanged exclusively through digital media. At this point nowadays there are three main varieties of digital currency: Cryptocurrency, Stablecoins, and central bank digital currency.

## Benefits of Digital Currency

- **Faster Payments** : Using Digital Currency, you can complete payments much faster than current means, which can take days for financial institutions to confirm a transaction.
- **Cheaper International Transfers** : International Currency Transactions are very expensive. Individuals are charged high fees to move funds from one country to another, especially when it involves currency conversions. Digital assets could disrupt this market by making it faster and less costly.
- **24 / 7** : Existing money transfers often take more time during weekends and outside normal business hours because banks are closed and can’t confirm transactions. With digital currency, transactions work at the same speed 24 hours a day, seven days a week.
- **More efficient government payments** : If the government developed a Payment System, it could send payments like tax refunds, child benefits and food stamps to people instantly, rather than trying to mail them a check or try with prepaid debit cards.

## Drachma

In the history of the Hellenic Republic, the drachma was an ancient monetary unit issued in many city-states over a period of ten centuries, from the Archaic period throughout the classical period, the Hellenistic period, the Hellenistic period to the Roman period under Greek Imperial coinage. The ancient drachma originated in Greece around the 7th century BC. The coin is usually made of silver or sometimes of gold. The drachma was unique to each city-state that minted them, and they were sometimes circulated throughout the Mediterranean. The currency of Athens was considered the strongest and became the most popular.

## Properties and Characteristics of Drachma

The name of drachma is derived from the verb drasma, believed to be the same word in the sense of the handful or handle of Mycenaean Pylos. The characteristics of the drachma are that they are subject to categories of smaller currencies. 

- **Drachma** : 1 drx or 7 obols
- **Obols**: Smaller amount of coins – 6 obols

Obols were originally used as a form of early currency, starting around 1100 BC. and is a form of gold usually copper, iron, expressed by weight in a developed system of exchange.

**Ancient Greek** coins usually had distinctive names in everyday use. The Athenian tetradrachm was called an owl, the Aeginean was called cheloni (Turtle), The Corinthian Stater was called hippos (horse), and so on. Each city would Mint its own currency, but it would be subject to the drachma and stamped with recognizable symbols of the city.

## Digital Drachma

The digital drachma will be exactly the same as the normal one, as mentioned above and will have exactly the same characteristics and peculiarities, but all this now digital.

### Features Of Digital Drachma

- Currency: **Drachma**
- Digital Name: **DRX**
- Digital Currency: 1 DRX
- Subcurrency: 6 OBL

## Conclusion

The drachma has dominated from antiquity until today with many peculiarities and changes and can be used and transformed into many systems of use of both trade and exchange of goods and services.

## App

<p align="center"><img src="Ancient_Greek_Coin.png" alt="Ancient_Greek_Coin" width="200" height="200"/></p>